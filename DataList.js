import React, { useState } from 'react';

export default function DataList({
	labelText,
	required,
	className,
	type,
	placeholder,
	value,
	name,
	onChange,
	onFocus,
	min,
	max,
	id,
	click,
}) {
	const [info, setInfo] = useState({
		user_name: '',
		township_id: '',
	});
	const [data, setData] = useState([
		{ id: 1, name: 'mandalay' },
		{ id: 2, name: 'yangon' },
		{ id: 3, name: 'naypyitaw' },
	]);
	const [display, setDisplay] = useState(false);
	const [townshipName, setTownshipName] = useState('');

	const handleChange = (e) => {
		e.preventDefault();
		let { name, value } = e.target;
		if (name === 'township_id') {
			setTownshipName(value);
			let township = data.find((township) => township.name === value);
			if (township) {
				setDisplay(false);
				setInfo({
					...info,
					[name]: township.id,
				});
			}
		} else {
			setInfo({
				...info,
				[name]: value,
			});
		}
	};

	return (
		<>
			<div>
				Name:::
				<input type="text" name="user_name" value={info.user_name} onChange={handleChange} />
				Townships:::
				<input
					className={`block focus:outline-none h-10 px-4 bg-gray-100 rounded-lg focus:shadow-inner focus:bg-white border border-gray-400 ${
						className ? className : null
					}`}
					type="text"
					placeholder="Mandalay"
					value={townshipName}
					name="township_id"
					onChange={handleChange}
					onClick={() => setDisplay(!display)}
					onFocus={onFocus}
					required={required}
					min={min}
					max={max}
					id={id}
				/>
				{display && (
					<div className="autoContainer">
						{data
							.filter((df) => df.name.includes(townshipName.toLocaleLowerCase()))
							.map((v, i) => {
								return (
									<option
										key={v.id}
										id={v.id}
										onClick={() => {
											setTownshipName(v.name);
											setDisplay(false);
											setInfo({
												...info,
												township_id: v.id,
											});
										}}
									>
										{v.name}
									</option>
								);
							})}
					</div>
				)}
			</div>
			<button onClick={(e) => console.log(info)}>Look Console</button>
		</>
	);
}
