
function myFunction() {
  var txt;
  var person = prompt("Please enter your name:", "Harry Potter");
  //prompt box
  if (person == null || person == "") {
    //check empty in the prompt box
    txt = "User cancelled the prompt";
  } else {
    //text has in the prompt box
    txt = "Hello " + person + "! How are you today?";
  }
  document.getElementById("demo").innerHTML=txt;
}

function myConfirm(){
    var con=confirm("Do you want to leave this page");
    //confirm box
    if(con==true)
    //check
        alert("Thank");
    else 
        alert("No Thank");
}

function removeMessage(){
  var parent=document.getElementById("demo2");//parent id
  var child=document.getElementById("txt");//remove message's id
  parent.removeChild(child);
  //remove message
}

function replaceMessage(){
  var p=document.createElement("p");
  var node=document.createTextNode("This is new...");
  p.appendChild(node);
  //adding text to the paragraph

  var parent=document.getElementById("demo2");
  var child=document.getElementById("txt2");//old text
  parent.replaceChild(p,child);
  //replace text
}